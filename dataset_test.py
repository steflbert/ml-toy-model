import numpy as np
import dataset
import unittest


class OneHotTest(unittest.TestCase):
# Test dense to onehot

    #test it for just one label
    def test_onelabel(self):
        labels = np.array([3], dtype=int)
        labels_onehot = np.array([[0, 0, 0, 1, 0, 0, 0, 0, 0, 0]], dtype=int)
        output = dataset.dense_to_one_hot(labels)
        self.assertTrue((np.all(output == labels_onehot)),
                        f"{labels_onehot} expected, not {output}")

    # test with multiple labels
    def test_multiple(self):
        labels = np.array([3, 7], dtype=int)
        labels_onehot = np.array(
            [[0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]], dtype=int)
        output = dataset.dense_to_one_hot(labels)
        self.assertTrue((np.all(output == labels_onehot)),
                        f"{labels_onehot} expected, not {output}")

    # test if an error is raised when using too high labels
    def test_error(self):
        labels = np.array([11], dtype=int)
        with self.assertRaises(ValueError):
            dataset.dense_to_one_hot(labels, num_classes=10)


if __name__ == "__main__":
    unittest.main()
