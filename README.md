This is a small framework I wrote for building and training neural networks. I wrote it to test my understanding of neural networks. It is way too slow to actually use it for anything.

At some point, I want to increase performance. Right now, the bottleneck are the convolutions, especially in backprop.