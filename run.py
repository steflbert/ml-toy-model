# %% setup and import
# %load_ext autoreload
# %autoreload 2

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

import dataset as ds
import model as m


def plotimagewithlabel(axes, image, label, truelabel, num_classes=10):
    '''Plots an image and the coressponding labels
    
    Args:
        axes: the axes to plot onto
        image: the grayscale image
        label: array with the calculated labels
        truelabel: one-hot array with true labels'''

    axes[0].imshow(np.squeeze(image), cmap='Greys')
    axes[0].set_axis_off()

    #see if label was correct
    if np.argmax(label) == np.argmax(truelabel):
        axes[1].set_title('TRUE', color = 'green')
        barcolor = 'green'
    else:
        axes[1].set_title('FALSE', color = 'red')
        barcolor = 'red'
    colors = np.array(['lightblue']*num_classes)
    # set right class red
    colors[np.argmax(truelabel)] = barcolor
    axes[1].bar(
        np.arange(num_classes),
        label,
        color=colors
    )
    axes[1].set_xticks(np.arange(num_classes))
    axes[1].set_xlabel('Class')
    axes[1].set_ylabel('Probability')

def plotgrid(images, labels, truelabels, num_classes=10, cols=2, title=''):
    '''Plots grids of images and the coressponding labels
    
    Args:
        images: the grayscale images
        labels: array with the calculated labels
        truelabel: one-hot array with true labels
        cols: number of columns to plot'''
    #get the number of plots
    n = images.shape[-1]

    #see if there are less images than columns
    if n < cols:
        cols = n

    nrows = int(np.ceil(n/cols))

    #there are two plots per image
    fig, axes = plt.subplots(nrows=nrows, ncols=cols*2)

    for i in range(n):
        #plot image
        if nrows == 1:
            current_axes = [axes[2*i], axes[2*i+1]]
        else:
            row = i//cols
            #define axis to plot into
            current_axes = [axes[row, 2*(i % cols)], axes[row, 2*(i % cols)+1 ]]
        image = images[..., 0, i]
        label = labels[..., i]
        t_label = truelabels[..., i]
        plotimagewithlabel(current_axes, image, label, t_label, num_classes)

    fig.suptitle(title)

    fig.set_figheight(2.4*nrows)
    fig.set_figwidth(4.8*cols)

    fig.tight_layout()

def preprocess(input):
    '''preprocess in put by scaling it to a range between 0 and 1'''
    return input / 255 - np.mean(input)

def train(model, images, labels, epochs, bs=32, preprocess=preprocess):
    #preprocess images
    images_prep = preprocess(images)

    nsamples = images.shape[-1]
    #number of samples divisibke by batch size
    nsamples_bs = nsamples - (nsamples % bs)
    #number of batches
    nbatches = nsamples_bs // bs

    #make random number generator
    rng = np.random.default_rng()

    #array for loss
    loss = np.zeros((epochs, nbatches))

    #with counter
    t_epoch = tqdm(range(epochs), position=1)
    for i in t_epoch:

        #generate shuffle indeces
        samples = np.arange(nsamples, dtype='int')
        rng.shuffle(samples)
        #and cut to multiple of batch size and reshape to get batches
        samples = samples[:nsamples_bs].reshape((-1, bs))

        #second loop for batches
        t_batch = tqdm(range(nbatches), position=0)
        for b in t_batch:
            
            #do forward pass
            result = model.forward(images_prep[...,samples[b]])

            #do backward pass
            l = model.backward(labels[...,samples[b]])

            #update loss
            loss[i,b] = l

            #update progress bar for batch
            t_batch.set_description(desc=f'loss: {l:3.4f}', refresh=True)
        #update epoch progress bar
        desc = f'loss: {np.mean(loss[i]):3.4f}'
        t_epoch.set_description(desc=desc, refresh=True)

    #plot loss
    plt.plot(loss.flatten())
    plt.title('Loss')
    plt.xlabel('Iteration')
    plt.ylabel('Loss')

    #show image
    plt.show()
    plt.close()

    #define number of plats
    nplots = min(8, bs)

    #plot result
    plotgrid(
        images=images[...,:nplots],
        labels=result[...,:nplots],
        truelabels=labels[...,:nplots],
        title=f'Final result'
        )

    #show image
    plt.show()
    plt.close()

    #graph weights and biases

    fig, axes = plt.subplots(nrows=1, ncols=2)
    axes[0].hist(model.layers[0].grad_weights.flatten())
    axes[0].set_title('Gradients of Weights')

    axes[1].hist(model.layers[0].grad_biases.flatten())
    axes[1].set_title('Gradients of Biases')

    plt.show()
    plt.close()

    #graph weights and biases

    fig, axes = plt.subplots(nrows=1, ncols=2)
    axes[0].hist(model.layers[0].weights.flatten())
    axes[0].set_title('Weights')

    axes[1].hist(model.layers[0].biases.flatten())
    axes[1].set_title('Biases')

    plt.show()
    plt.close()

# %% load dataset

train_images_file = 'MNIST/train-images-idx3-ubyte.gz'
train_labels_file = 'MNIST/train-labels-idx1-ubyte.gz'
test_images_file = 'MNIST/t10k-images-idx3-ubyte.gz'
test_labels_file = 'MNIST/t10k-labels-idx1-ubyte.gz'

nclasses = 10

training_images = ds.extract_images(train_images_file)
training_labels = ds.dense_to_one_hot(
    ds.extract_labels(train_labels_file),
    num_classes=nclasses
)

test_images = ds.extract_images(test_images_file)
test_labels = ds.dense_to_one_hot(
    ds.extract_labels(test_labels_file),
    num_classes=nclasses
)

# %% build LeNet

hyperparams = {
    'fsize' : 3,
    'learning_rate' : 1e-4,
    'weight_iniscale' : 1e-3,
    'epochs' : 20,
    'batch_size' : 1,
    'optimizer' : 'SGD',
    'rho1' : 0.95,
    'rho2' : 0.99
    }

input_shape = 28 + hyperparams['fsize'] - 1
#padding
p = (hyperparams['fsize'] - 1) // 2

#reshape for batches
training_images_padded = np.pad(training_images, ((0,0), (0,0), (p,p), (p,p)))
#add channel dimension
shape = (1, -1, input_shape, input_shape)
training_images_reshaped = training_images_padded.reshape(shape)
#transpose to get the batch dimension in the back
training_images_reshaped = training_images_reshaped.transpose((2, 3, 0, 1))

#also transpose labels
training_labels_reshaped = np.copy(training_labels)
training_labels_reshaped = training_labels_reshaped.transpose((1, 0))
    
#calculate first layer width (after maxpool)
w1 = (input_shape + 1 - hyperparams['fsize']) // 2
#second layer
w2 = (w1 + 1 - hyperparams['fsize']) // 2

layers = [
    #first conv block
    #first conv layer with image as input, output: (28, 28, 6)
    m.ConvLayer(n_filters=6, input_depth=1, kernel_size=hyperparams['fsize'],
        ini_bias=0, weight_iniscale=hyperparams['weight_iniscale']),
    #relu layer
    m.ReLuLayer(),
    #pooling layer, output: (14, 14 ,6)
    m.MaxpoolLayer(size=2),

    #second conv block
    #first conv layer, output: (16, 10, 10)
    m.ConvLayer(n_filters=16, input_depth=6, kernel_size=hyperparams['fsize'],
        ini_bias=0, weight_iniscale=hyperparams['weight_iniscale']),
    #relu layer
    m.ReLuLayer(),
    #pooling layer, output: (5, 5, 16)
    m.MaxpoolLayer(size=2),

    #first full layer, output: (120,)
    m.FullLayer(n_neurons=120, input_size=16*w2*w2, ini_bias=0,
        weight_iniscale=hyperparams['weight_iniscale']),
    #relu layer
    m.ReLuLayer(),

    #second full layer, output: (84,)
    m.FullLayer(n_neurons=84, input_size=120, ini_bias=0,
        weight_iniscale=hyperparams['weight_iniscale']),
    #relu layer
    m.ReLuLayer(),

    #output layer, output: (10,)
    m.FullLayer(n_neurons=10, input_size=84, ini_bias=0,
        weight_iniscale=hyperparams['weight_iniscale']),

]

lenet = m.CNN(
    layers=layers,
    learning_rate=hyperparams['learning_rate'],
    optimizer=hyperparams['optimizer'],
    optimizer_params={'rho1':hyperparams['rho1'], 'rho2':hyperparams['rho2']},
    silent=True)

train(
    model=lenet,
    images=training_images_reshaped[:,:,:,:100],
    labels=training_labels_reshaped[:,:100],
    epochs=hyperparams['epochs'],
    bs=hyperparams['batch_size']
)

# %% learn larger for testing

layers = [m.FullLayer(n_neurons=2, input_size=2, ini_bias=0,
        weight_iniscale=hyperparams['weight_iniscale']),
        m.ReLuLayer(),
        m.FullLayer(n_neurons=2, input_size=2, ini_bias=0,
        weight_iniscale=hyperparams['weight_iniscale'])]

largernet = m.CNN(
    layers=layers,
    learning_rate=1e-2,
    optimizer='SGD',
    optimizer_params={'rho1':hyperparams['rho1'], 'rho2':hyperparams['rho2']},
    silent=True)

epochs = 10000

loss = np.empty(epochs)

silent = True

for i in range(epochs):
    #make samples
    samples = np.random.normal(size=(2, 2))
    #apply larger
    label = np.zeros(samples.shape)
    label[np.argmax(samples, axis=0)] = 1

    if not silent:
        print(f'Epoch:  {i}')

        print(f'Sample: {samples[0,0]:+1.3f} {samples[1,0]:+1.3f}')
        print(f'Label:  {label[0,0]:+1.3f} {label[1,0]:+1.3f}')

    #do forward pass
    result = largernet.forward(label)

    if not silent:
        print(f'Result: {result[0,0]:+1.3f} {result[1,0]:+1.3f}\n')
        
        print(f'     Bias:    {largernet.layers[0].biases[0]:+1.3f} '
            + f'{largernet.layers[0].biases[1]:+1.3f}')
        print(f'     Weights: {largernet.layers[0].weights[0,0]:+1.3f} '
            + f'{largernet.layers[0].weights[0,1]:+1.3f} '
            + f'{largernet.layers[0].weights[1,1]:+1.3f} '
            + f'{largernet.layers[0].weights[1,1]:+1.3f}')

        print('Do backard pass')

    #do backward pass
    l = largernet.backward(label)

    if not silent:
        print(f'loss: {l:1.3f}')
        print(f'Grad Bias:    {largernet.layers[0].grad_biases[0]:+1.3f} '
            + f'{largernet.layers[0].grad_biases[1]:+1.3f}')
        print(f'Grad Weights: {largernet.layers[0].grad_weights[0,0]:+1.3f} '
            + f'{largernet.layers[0].grad_weights[0,1]:+1.3f} '
            + f'{largernet.layers[0].grad_weights[1,1]:+1.3f} '
            + f'{largernet.layers[0].grad_weights[1,1]:+1.3f}')

        print(f'     Bias:    {largernet.layers[0].biases[0]:+1.3f} '
            + f'{largernet.layers[0].biases[1]:+1.3f}')
        print(f'     Weights: {largernet.layers[0].weights[0,0]:+1.3f} '
            + f'{largernet.layers[0].weights[0,1]:+1.3f} '
            + f'{largernet.layers[0].weights[1,1]:+1.3f} '
            + f'{largernet.layers[0].weights[1,1]:+1.3f}\n')

    #update loss
    loss[i] = np.mean(l)


plt.plot(loss)
plt.show()
plt.close()
# %%
