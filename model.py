from abc import ABCMeta, abstractmethod

import numpy as np
from scipy.signal import fftconvolve


def softmax(input):
    '''Perform softmax over the input

    Args:
    input: softmax is performed only over the first dimension
    '''
    #remove maximum for better stability
    input = np.exp(input - np.max(input, axis = 0))
    input = input / np.sum(input, axis = 0)
    return input

class CNN:
    '''CNN, consists of multiple layers, only consective layers are connected
    The same functions as in the Layers are implemented and act on all Layers
    A softmax is automatically added at the end

    Args:
        layers: list of layers, each element should be a *Layer object
        loss: The loss that should be used. Cross-entropy is the default and 
            currently only possible option
        optimizer: the optimizer that should be used, SGD (stochastic gradient
            descent) is the only possible option right now.
        learning rate: The learning rate to use, can also be changed later by
            changing self.learning_rate
        silent: if True, no output is produced
    '''

    def __init__(self, layers, loss='crossentropy', optimizer='SGD',
        optimizer_params = {}, learning_rate = 1e-5, silent = True):
        self.layers = np.array(layers)
        self.losstype = loss
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.silent = silent

        #initialize optimizer (if not SGD)
        if optimizer == 'Adam':
            #initialize estimators
            for l in np.arange(self.layers.size, dtype=int):
                if hasattr(self.layers[l], 'biases'):
                    self.layers[l].s_bias = np.zeros(layers[l].biases.shape)
                    self.layers[l].r_bias = np.zeros(layers[l].biases.shape)
                if hasattr(self.layers[l], 'weights'):
                    self.layers[l].s_weight = np.zeros(layers[l].weights.shape)
                    self.layers[l].r_weight = np.zeros(layers[l].weights.shape)
            self.rho1 = optimizer_params['rho1']
            self.rho2 = optimizer_params['rho2']
            #initialize t
            self.t = 0
        elif optimizer == 'SGD':
            pass
        else:
            raise ValueError(f'Optimizer {optimizer} unknown')


        #small constant for stability
        self.eps = 1e-7

    def forward(self, input):
        for l in self.layers:
            #pass output as input to the next layer
            if not self.silent:
                print(f'Running layer {l}')
            input = l.forward(input)
            if not self.silent:
                print(f'Output of shape {input.shape}\n')

        #remember output (which is confusingly now called input)
        self.output = input

        #apply softmax
        input = softmax(input)

        #return result
        return input

    def calculate_gradients(self, true_labels):
        '''Calculate the gradients
        
        Args:
            true_labels: The true label vector'''

        #calculate the fist gradient and loss
        if self.losstype == 'crossentropy':
            loss = self.loss_crossentropy(true_labels)
            #calculate gradient
            gradient = self.gradient_crossentropy(true_labels)

        #iterate over layers in reverse order
        for l in np.flip(np.copy(self.layers)):
            #get jacobien of input
            if not self.silent:
                print(f'Running layer {l} backwards')
            gradient = l.gradient(gradient)
            if not self.silent:
                print(f'Gradient of shape {gradient.shape}\n')

        return loss

    def loss_crossentropy(self, true_labels):
        '''Calulates mean cross entropy loss over batch

        Args:
            true_labels: The true labels as one-hot vectors'''
        #calculate probabilities
        probs = softmax(self.output)

        loss = - np.sum(true_labels * np.log(probs), axis = 0)
        total_loss = np.sum(loss)

        return total_loss

    def gradient_crossentropy(self, true_labels):
        #calculate probabilities
        probs = softmax(self.output)

        gradient = probs - true_labels
        return gradient

    def update(self):
        '''Do the parameter update according to the optimizer'''
        #iterate over layers in reverse order
        for l in np.flip(np.arange(self.layers.size, dtype=int)):
            layer = self.layers[l]
            if not self.silent:
                print('Updates are being applied\n')
            if self.optimizer == 'SGD':
                #update biases (if present)
                if hasattr(layer, 'grad_biases'):
                    layer.biases -= self.learning_rate*layer.grad_biases
                #update weights (if present)
                if hasattr(layer, 'grad_weights'):
                    layer.weights -= self.learning_rate*layer.grad_weights
            elif self.optimizer == 'Adam':
                #update t
                self.t += 1
                #update biases (if present)
                if hasattr(layer, 'grad_biases'):
                    #update first order
                    layer.s_bias = (self.rho1*layer.s_bias
                        + (1-self.rho1)*layer.grad_biases)
                    #correct it
                    layer.s_bias = layer.s_bias / (1 - self.rho1**self.t)
                    #update second order
                    layer.r_bias = (self.rho2*layer.r_bias
                        + (1-self.rho2)*np.square(layer.grad_biases))
                    #correct it
                    layer.r_bias = layer.r_bias / (1 - self.rho2**self.t)
                    #apply the update
                    layer.biases -= (self.learning_rate*layer.s_bias
                        / (np.sqrt(layer.r_bias) + 1e-8))
                #update weights (if present)
                if hasattr(layer, 'grad_weights'):
                    #update first order
                    layer.s_weight = (self.rho1*layer.s_weight
                        + (1-self.rho1)*layer.grad_weights)
                    #correct it
                    layer.s_weight = layer.s_weight / (1 - self.rho1**self.t)
                    #update second order
                    layer.r_weight = (self.rho2*layer.r_weight
                        + (1-self.rho2)*np.square(layer.grad_weights))
                    #correct it
                    layer.r_weight = layer.r_weight / (1 - self.rho2**self.t)
                    #apply the update
                    layer.weights -= (self.learning_rate*layer.s_weight
                        / (np.sqrt(layer.r_weight) + 1e-8))


    def backward(self, true_labels):
        '''Do the backward pass
        
        Args:
            true_labels: The true label vector'''

        #calculate the gradients
        loss = self.calculate_gradients(true_labels)
        #update the gradients
        self.update()

        return loss


#define an bastract class for the layers
class AbstractLayer(metaclass=ABCMeta):

    #it should have a method to perform forward passes
    @abstractmethod
    def forward(self, input):
        return

    #it should have a method to update the gradient
    @abstractmethod
    def gradient(self, gradient):
        return

class ConvLayer(AbstractLayer):
    '''Convolutional layer

    Args: 
        n_filters: number of kernels that are used
        input_depth: number of feature maps in the input
        kernel_size: size of th square kernel, size is (s x s)
        weight_iniscale: scale of the normal distribution for the weights
    There is no stride'''

    def __init__(self, n_filters, input_depth, kernel_size=3, ini_bias = 0.1,
        weight_iniscale = 0.01):
        self.n_filters = n_filters
        self.kernel_size = kernel_size
        self.input_depth = input_depth
        self.ini_bias = ini_bias
        self.weight_iniscale = weight_iniscale

        #initialize weights
        self.initialize_weights()

    def initialize_weights(self):
        '''initialize all weights to zero and biases to 0.1'''
        # initialize as many kernels as depth with kernel size
        weightshape = (self.kernel_size, self.kernel_size,
            self.input_depth, self.n_filters)
        self.weights = np.random.normal(size = weightshape)*self.weight_iniscale
        self.biases = np.ones((self.n_filters))*self.ini_bias

    def forward(self, input):
        '''Do a forward pass.

        args: input, of size (height, width, channels, batchsize)
        
        returns:
            output: size (height, width, nfilter, batchsize)'''

        #remember input
        self.input = np.copy(input)

        # check input
        if self.input_depth != input.shape[2]:
            raise ValueError('Input not of expected size, '+
                f'{input.shape[2]} instead of {self.input_depth}')
        if self.input.ndim != 4:
            raise ValueError(f'Input should have dim 4 not {self.input.ndim}')
        if np.any(np.array(input.shape[:2]) < self.kernel_size):
            raise ValueError('Input feature map too small')
        

        #inputshape is (h, w, depth, batchsize)
        #add dimension for nfilters
        input_edim = np.expand_dims(input, axis=3)

        #weightshape is (size, size, depth, nfilter)
        #add dimension for b
        weights_edim = np.expand_dims(self.weights, axis=4)

        #do the convolution
        conv = fftconvolve(input_edim, weights_edim, mode='valid', axes=(0,1))
        
        #sum over the depth
        output = np.sum(conv, axis=2)

        #add bias
        output = output + self.biases.reshape((1, 1, -1, 1))

        return output

    def gradient(self, gradient):
        '''This function takes the output gradient as input and calculates
        the gradient of its input and all its parameters

        Args:
            gradient: numpy array, shape should be output.shape

        Returns:
            input_gradient: The gradient of the input
        '''

        #backprop of biases is very simple, it is just the sum of the gradiens
        #of that feature map
        self.grad_biases = np.sum(gradient, axis=(0, 1, -1))

        #calculate weights
        #the derivative is the convolution with only one part derived
        #doing this for each weight takes a while
        #outshape is (h, w, nfilters, batchsize)
        #weightshape is (size, size, depth, nfilter)

        self.grad_weights = np.zeros(self.weights.shape)
        #add dimension for nfilters
        input_edim = np.expand_dims(self.input, axis=3)

        #add extra dimension in the gradient to reverse the sum
        grad_edim = np.expand_dims(gradient, axis=2)
        grad_edim = np.repeat(grad_edim, self.input_depth, axis=2)

        #loop over both weight dimensions
        for i in range(self.kernel_size):
            for j in range(self.kernel_size):
                kernels = np.zeros((self.weights.shape) + (1,))
                #set current weight to one (the derivative)
                kernels[i, j, ...] = 1
                #do convolution
                conv = fftconvolve(
                    input_edim, kernels, mode='valid', axes=(0,1)
                    )
                #multiply with gradient
                #sum over height, width and batch
                grad = np.einsum('hwdnb,hwdnb->dn', conv, grad_edim)
                #shape should be depth, nfilter
                self.grad_weights[i,j] = grad

        #define gradient of the input
        input_grad = np.zeros(self.input.shape)

        #add dimension for b
        weights_edim = np.expand_dims(self.weights, axis=4)
        expanded_inputshape = (self.input.shape[:-1]) + (1,self.input.shape[-1])

        #do the same as for the gradients, but with the input
        #loop over both weight dimensions
        for i in range(self.input.shape[0]):
            for j in range(self.input.shape[1]):
                input_one = np.zeros(expanded_inputshape)
                #set current weight to one (the derivative)
                input_one[i, j, ...] = 1
                #do convolution TODO: could maybe be replaced by simpler sum
                conv = fftconvolve(
                    input_one, weights_edim, mode='valid', axes=(0,1)
                    )
                #multiply with gradient
                #sum over height, width and nfilters
                grad = np.einsum('abcde,abcde->ce', conv, grad_edim)
                #shape should be depth, nfilter
                input_grad[i, j] = grad


        return input_grad

class FullLayer(AbstractLayer):
    '''Full Layer

    Args:
        n_neurons: number of neurons in the full layer
        input_size: number of input neurons to initialize weights
        weight_iniscale: scale of the normal distribution for the weights'''

    def __init__(self, n_neurons, input_size, ini_bias=0.1, weight_iniscale=.1):
        self.n_neurons = n_neurons
        self.input_size = input_size
        self.ini_bias = ini_bias
        self.weight_iniscale = weight_iniscale

        # initialize weights
        self.initialize_weights()

    def initialize_weights(self):
        # initialize weights
        weightshape = (self.n_neurons, self.input_size)
        self.weights = np.random.normal(size = weightshape)*self.weight_iniscale
        # initialize bias
        self.biases = np.ones(self.n_neurons)*self.ini_bias

    def forward(self, input):
        '''Do a forward pass.
        args:
            input: of size (features, batchsize),
            if not, it will be flattened
            
        returns:
            flattend output, with (features, batchsize)'''

        #check input
        if input.ndim < 2:
            raise ValueError('Input should have 2 or more dimensions')
        
        #remember input shape
        self.inputshape = input.shape

        #reshape input
        inputflat = input.reshape(-1, input.shape[-1])

        #check size
        if inputflat.shape[0] != self.input_size:
            raise ValueError(f'Input not of expected size ({inputflat.shape[0]}'
            + f' instead of {self.input_size})')

        #remember input
        self.inputflat = np.copy(inputflat)

        # flatten input and multiply
        output = np.dot(self.weights, inputflat)
        #add bias
        output = output + self.biases.reshape((-1, 1))
        return output

    def gradient(self, gradient):
        '''This function takes the output gradient as input and calculates
        the gradient of its input and all its parameters

        Args:
            gradient: numpy array, shape should be output.shape
        '''
    
        #backprop of biases is very simple, it is just the sum of the gradient
        self.grad_biases = np.sum(gradient, axis=-1)

        #backprop of the weights
        self.grad_weights = np.dot(gradient, self.inputflat.T)

        #backprop of the input is also very simple, it is just the weights
        return np.dot(self.weights.T, gradient).reshape(self.inputshape)

class MaxpoolLayer(AbstractLayer):
    '''Maxpool Layer

    Args:
        size: size of the pooling area, stride is set to size
    assumes that the last two input dimensions are divisble by size
    '''

    def __init__(self, size = 2):
        self.size = int(size)

    def forward(self, input):
        '''Do a forward pass.
        args:
            input, of size (height, width, depth, batchsize),
            if not, it will be flattened
            
        returns:
            output with height and width reduced'''
        
        #check input
        if input.ndim != 4:
            raise ValueError(f'Input should have 4 dims, not {input.ndim}')
        
        #define baseshape (shape except last two inputs), width and height
        h, w, d, bs  = input.shape
        
        if w % self.size != 0:
            raise ValueError('Width not divisible by size')
        if h % self.size != 0:
            raise ValueError('Height not divisible by size')

        #remember input
        self.input = np.copy(input)
        
        new_w = int(w / self.size)
        new_h = int(h / self.size)

        #reshape to have last row as stride size (put rest in first dim)
        stride_size = input.reshape((h, new_w, self.size, d, bs))
        #transpose
        stride_size = stride_size.transpose((1, 0, 2, 3, 4))
        
        #reshape to group squares together
        #switch w and h, because transposed
        squares = stride_size.reshape((new_w, new_h, self.size**2, d, bs))
        #transpose back
        squares = squares.transpose((1, 0, 2, 3, 4))

        #remember squares for backprop later
        self.squares = np.copy(squares)

        #take max
        maximum = np.max(squares, axis=2)

        return(maximum.reshape((new_h, new_w, d, bs)))

    def gradient(self, gradient):
        '''This function takes the output gradient as input and calculates
        the gradient of its input and all its parameters

        Args:
            gradient: numpy array, shape should be output.shape
        '''
        #gradient is the output gradient if it was used, 0 if not

        #define sizes
        new_h, new_w = self.squares.shape[:2]
        h = self.input.shape[0]
        d, bs = self.squares.shape[-2:]

        #see positions of maximum
        pos_max = np.expand_dims(np.argmax(self.squares, axis=2), axis=2)

        #define input gradient
        input_grad = np.zeros((new_h, new_w, self.size**2, d, bs))
        #expand the gradient of the output
        gradient_exp = np.expand_dims(gradient, axis=2)

        #set used positions to the corresponding gradient
        np.put_along_axis(input_grad, pos_max, gradient_exp, axis = 2)

        #put back in the original form
        input_grad = input_grad.reshape((h, new_w, self.size, d, bs))
        #transpose
        input_grad = input_grad.transpose((1, 0, 2, 3, 4))
        #resize again
        input_grad = input_grad.reshape((new_w, new_h, self.size**2, d, bs))
        #transpose back
        input_grad = input_grad.transpose((1, 0, 2, 3, 4))

        #put in desired shape
        input_grad = input_grad.reshape(self.input.shape)

        return input_grad

class ReLuLayer(AbstractLayer):
    '''
    Performs ReLU on the input. There are no arguments
    '''

    def forward(self, input):
        '''Do a forward pass.
        args:
            input: of size (..., batchsize)
        
        returns:
            output with inputsize'''


        #check input
        if input.ndim < 2:
            raise ValueError(
                f'Input should have 1 < dimensions, not {input.ndim}'
                )

        #remember input
        self.input = np.copy(input)

        #calculate ReLU
        input[input<0] = 0
        return input

    def gradient(self, gradient):
        '''This function takes the output gradient as input and calculates
        the gradient of its input and all its parameters

        Args:
            gradient: numpy array, shape should be output.shape
        '''
        #set gradients to zero where input was not used
        gradient = np.copy(gradient)
        gradient[self.input<0] = 0

        return gradient
