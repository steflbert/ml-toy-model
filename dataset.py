import numpy as np
import gzip
import matplotlib.pyplot as plt


def read32(bytestream):
    dt = np.dtype(np.uint32).newbyteorder('>')
    return np.frombuffer(bytestream.read(4), dtype=dt)[0]


def dense_to_one_hot(labels, num_classes=10):
    """Converts labels to one hot vector.
    Args:
      labels: array to convert
      num_classes: number of classes, default is 10
    Returns:
      new int array with extra dimension of size num_classes at the end
      containing a one-hot vector
    Raise:
      ValueError: If label has hihger number than number of classes
    """
    # make new array with one dimesnion more
    labels_onehot = np.zeros(labels.shape + (num_classes,), dtype=int)
    # see if all labels do not exceed class number
    if np.any(labels > num_classes):
        raise ValueError("label number exceeds class number")
    # flatten both array, but add an offset to the labels
    offset = np.arange(labels.size)*num_classes
    labels_onehot.flat[labels.flat + offset] = 1
    return labels_onehot


def extract_images(f):
    """Extract the images into a 4D uint8 numpy array [index, y, x, depth].
    Args:
      f: A file object that can be passed into a gzip reader.
    Returns:
      data: A 4D uint8 numpy array [index, y, x, depth].
    Raises:
      ValueError: If the bytestream does not start with 2051.
    """
    print('Extracting', f)
    with gzip.GzipFile(f) as bytestream:
        magic = read32(bytestream)
        if magic != 2051:
            raise ValueError('Invalid magic number %d in MNIST image file: %s' %
                             (magic, f))
        num_images = read32(bytestream)
        rows = read32(bytestream)
        cols = read32(bytestream)
        buf = bytestream.read(rows * cols * num_images)
        data = np.frombuffer(buf, dtype=np.uint8)
        #add dimension one, because there is only one channel
        data = data.reshape(num_images, 1, rows, cols)
        return data


def extract_labels(f, one_hot=False, num_classes=10):
    """Extract the labels into a 1D uint8 numpy array [index].
    Args:
      f: A file object that can be passed into a gzip reader.
      one_hot: Does one hot encoding for the result.
      num_classes: Number of classes for the one hot encoding.
    Returns:
      labels: a 1D uint8 numpy array.
    Raises:
      ValueError: If the bystream doesn't start with 2049.
    """
    print('Extracting', f)
    with gzip.GzipFile(f) as bytestream:
        magic = read32(bytestream)
        if magic != 2049:
            raise ValueError('Invalid magic number %d in MNIST label file: %s' %
                             (magic, f.name))
        num_items = read32(bytestream)
        buf = bytestream.read(num_items)
        labels = np.frombuffer(buf, dtype=np.uint8)
        if one_hot:
            return dense_to_one_hot(labels, num_classes)
        return labels
