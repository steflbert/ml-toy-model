import timeit
import unittest

import numpy as np

import model

def set_seed(seed = 0):
    '''Set the seed for all random numbers to a fixed value

    Args:
        Seed: The seed to set, default is 0.
    '''
    np.random.seed(seed)

def compare(output, expected_output, accuracy = 1e-5):
    '''Compare two outputs to see if they match to a certain accruacy

    Args:
        output: The generated output as numpy array
        expected_output: The output that was calculated as numpy array
        accuracy: The maximum allowed difference (due to rounding errors)
    '''

    #compare the shape
    if output.shape != expected_output.shape:
        result = False
        message = (f'Shape do not match, expected {expected_output.shape}, '
            + f' not {output.shape}'
            )
        return result, message

    #compare numerical values
    diff = np.abs(output - expected_output)
    result = np.all(diff < accuracy)
    
    #get the significant digit
    digits = np.empty(3, dtype = int)
    vals = [diff, output, expected_output]
    #take logarithm (with small constant to not have infinities)
    digits = - np.log10(np.array([np.max(np.abs(x)) for x in vals]) + 1e-8)
    #replace negative numbers
    digits[digits < 0] = 0
    #take maximum
    #take the most digits of all three (to see all outputs)
    digits = int(np.ceil(np.max(digits))) + 1
    #round to significant digits
    sig_dig = lambda x : np.round(x, decimals=digits)
    
    #write message
    message = (f'Biggest difference was {sig_dig(np.max(diff))}\n' +
        f'diff:\n{sig_dig(diff)}\n' +
        f'output:\n{sig_dig(output)}\n' +
        f'expected output:\n{sig_dig(expected_output)}'
        )

    return result, message

def numerical_jac_input(layer, input):
    '''Calculates the jacobien with given layer and input
    The values are averaged over the batch

    Args:
        layer: layer to differentiate
        input: input to use
    '''
    #do forward pass
    center = layer.forward(input).flatten()

    #do not consider batch size in shape
    jac = np.zeros((center.size, input.size))

    #do numerical derivation
    stepsize = 1e-5

    for i in range(input.size):
        step = np.zeros(input.shape)
        #define infinitersimal element
        step.flat[i] = stepsize
        leftside = layer.forward(input-step).flatten()
        rightside = layer.forward(input+step).flatten()
        samples = np.vstack((leftside, center, rightside))
        #do gradient and take center
        grad = np.gradient(samples, stepsize, axis=0)[0]
        #calculate position of input
        pos = i
        jac[:,pos] += grad
    return jac

def get_input_gradients(layer, input):
    '''Calculates the gradients of the input numerically and using the gradient
    method
    
    Args:
        layer: The layer that should be used
        input: The input that should be used
        
    Returns:
        output: The algebraically calculated gradient
        expected_output: The numerically calculated gradient
        They both have the input shape'''
    #do forward pass
    nn_output = layer.forward(np.copy(input))

    #define previous gradient
    prev_grad = np.random.normal(size=nn_output.shape)

    #do backward pass
    output = layer.gradient(prev_grad)

    #get expected output
    jac = numerical_jac_input(layer, input)
    expected_output = np.dot(jac.T, prev_grad.flatten())
    expected_output = expected_output.reshape(input.shape)
    return output, expected_output

def numerical_jac_params(layer, input, paramtype):
    '''Calculates the jacobien with given layer and input
    The values are averaged over the batch

    Args:
        layer: layer to differentiate
        input: input to use
    '''
    #do forward pass
    center = layer.forward(input).flatten()

    #get sizes for selected parameters
    if paramtype == 'bias':
        #get shape and size
        num_params = layer.biases.size
        param_shape = layer.biases.shape
        #get original parameters
        original_param = np.copy(layer.biases)
        #define update function
        def update_param(layer, x):
            layer.biases = x

    elif paramtype == 'weight':
        #get shape and size
        num_params = layer.weights.size
        param_shape = layer.weights.shape
        #get original parameters
        original_param = np.copy(layer.weights)
        #define update function
        def update_param(layer, x):
            layer.weights = x
    else:
        raise ValueError(f'Parameter type {paramtype} not implemented')

    #calculated values
    jac = np.zeros((center.size, num_params))

    #do numerical derivation
    stepsize = 1e-5

    #loop over parameters
    for i in range(num_params):
        step = np.zeros(param_shape)
        #define infinitersimal element
        step.flat[i] = stepsize
        #update parameters
        update_param(layer, original_param - step)
        #do forward pass and average
        leftside = layer.forward(input).flatten()
        #update parameters
        update_param(layer, original_param + step)
        #do forward pass and average
        rightside = layer.forward(input).flatten()
        samples = np.vstack((leftside, center, rightside))
        #do gradient and take center
        grad = np.gradient(samples, stepsize, axis=0)[0]
        jac[:,i] = grad
    #restore original parameters
    update_param(layer, original_param)
    return jac

def numerical_grad_params_loss(model, layer, input, truelabels, paramtype):
    '''Calculates the jacobien with given layer and input
    The values are averaged over the batch

    Args:
        model: the model to differentiate
        layer: number of layer to differentiate
        input: the original input (for forard passes)
        truelabels: the true label that was used
        paramtype: if biases or weights should be differentiated
    '''
    #do forward pass
    model.layers[layer].forward(input)
    #do backward pass
    center = model.calculate_gradients(truelabels)

    #get sizes for selected parameters
    if paramtype == 'bias':
        #get shape and size
        num_params = model.layers[layer].biases.size
        param_shape = model.layers[layer].biases.shape
        #get original parameters
        original_param = np.copy(model.layers[layer].biases)
        #define update function
        def update_param(layer, x):
            layer.biases = x

    elif paramtype == 'weight':
        #get shape and size
        num_params = model.layers[layer].weights.size
        param_shape = model.layers[layer].weights.shape
        #get original parameters
        original_param = np.copy(model.layers[layer].weights)
        #define update function
        def update_param(layer, x):
            layer.weights = x
    else:
        raise ValueError(f'Parameter type {paramtype} not implemented')

    #calculated values
    grad = np.zeros((num_params))

    #do numerical derivation
    stepsize = 1e-5

    #loop over parameters
    for i in range(num_params):
        step = np.zeros(param_shape)
        #define infinitersimal element
        step.flat[i] = stepsize
        #update parameters
        update_param(model.layers[layer], original_param - step)
        #do forward pass
        model.forward(input)
        #do backward pass
        leftside = model.calculate_gradients(truelabels)
        #update parameters
        update_param(model.layers[layer], original_param + step)
        #do forward pass
        model.forward(input)
        #do backward pass
        rightside = model.calculate_gradients(truelabels)
        #construct samples
        samples = np.array([leftside, center, rightside])
        #do gradient and take center
        grad[i] = np.gradient(samples, stepsize, axis=0)[0]
    #restore original parameters
    update_param(model.layers[layer], original_param )
    return grad

class ModelTestConv(unittest.TestCase):
    def setUp(self):
        self.layer = model.ConvLayer(
            n_filters = 4,
            input_depth = 3,
            kernel_size = 5)

    def test_onelayer_ones_conv(self):
        #set weights to range
        self.layer.weights = np.ones(self.layer.weights.shape)

        bs = 8

        #set input to ones
        input = np.ones((5, 7, self.layer.input_depth, bs))

        #set expected output (calculated by hand)
        val = self.layer.input_depth*(self.layer.kernel_size**2)
        #define new size
        h = input.shape[0]+1-self.layer.kernel_size
        w = input.shape[1]+1-self.layer.kernel_size
        expected_output = np.ones((h, w, self.layer.n_filters, bs))*val
        #add bias
        expected_output += self.layer.ini_bias

        output = self.layer.forward(input)
        
        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_onelayer_range_conv(self):
        #set weights to range
        w_size = self.layer.weights.size
        w_shape = self.layer.weights.shape

        self.layer.weights = np.arange(w_size).reshape(w_shape)

        #set biases to range
        b_size = self.layer.biases.size
        b_shape = self.layer.biases.shape
        self.layer.biases = np.arange(b_size).reshape(b_shape)

        #set input to range
        bs = 8 #batch size
        ks = self.layer.kernel_size
        v9 = np.arange(bs*(ks**2)*self.layer.input_depth)
        input = v9.reshape((self.layer.input_depth, bs, ks, ks))
        #transpose (so that counting is along the input, not batch size)
        input = input.transpose((2, 3, 0, 1))

        #set expected output (calculated by hand)
        #note that the kernel is flipped
        nf = self.layer.n_filters
        expected_output = np.zeros((1, 1, nf, bs))
        for b in range(bs):
            for i in range(nf):
                for j in range(self.layer.input_depth):
                    #get input
                    input_line = input[:, :, j, b].flatten()
                    #get weights and flip (because of conv)
                    filter_line = np.flip(self.layer.weights[:,:,j,i].flatten())
                    expected_output[0, 0, i, b] += input_line.dot(filter_line)
        
        #add bias
        expected_output += self.layer.biases.reshape(1, 1, -1, 1)

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_input_conv(self):
        #define input
        bs = 8
        input = np.random.normal(size=(10, 6, self.layer.input_depth, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_bias_conv(self):
        #define input (without batch)
        bs = 8
        input = np.random.normal(size=(20, 8, self.layer.input_depth, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #do forward pass
        nn_output = self.layer.forward(np.copy(input))

        #define previous gradient
        prev_grad = np.random.normal(size=nn_output.shape)

        #calculate the gradients
        self.layer.gradient(prev_grad)

        #get expected output
        jac = numerical_jac_params(self.layer, input, 'bias')
        expected_output = np.dot(jac.T, prev_grad.flatten())
        expected_output = expected_output.reshape(self.layer.biases.shape)

        #compare the results
        result, message = compare(self.layer.grad_biases, expected_output)
        self.assertTrue(result, message)

    def test_backprop_weights_conv(self):
        #define input
        bs = 8
        input = np.random.normal(size=(20, 10, self.layer.input_depth, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #do forward pass
        nn_output = self.layer.forward(np.copy(input))

        #define previous gradient
        prev_grad = np.random.normal(size=nn_output.shape)

        #calculate the gradients
        self.layer.gradient(prev_grad)
        
        #get expected output
        jac = numerical_jac_params(self.layer, input, 'weight')
        expected_output = np.dot(jac.T, prev_grad.flatten())
        expected_output = expected_output.reshape(self.layer.weights.shape)

        #compare the results
        result, message = compare(self.layer.grad_weights, expected_output)
        self.assertTrue(result, message)

class ModelTestFull(unittest.TestCase):
    def setUp(self):
        self.layer = model.FullLayer(
            n_neurons = 10,
            input_size = 50
        )

    def test_range_full(self):
        #set weights to range
        weightshape = self.layer.weights.shape
        n_weights = self.layer.weights.size
        self.layer.weights = np.arange(n_weights).reshape(weightshape)

        #set biases
        self.layer.biases = np.arange(self.layer.n_neurons)

        #defien input
        bs = 8 #batch size
        input = np.arange(self.layer.input_size*bs).reshape(bs, -1)
        #transpose (so that counting is along the input, not batch size)
        input = input.transpose((1, 0))

        #define expected output
        expected_output = np.zeros((self.layer.n_neurons, bs))
        s = self.layer.input_size
        for b in range(bs):
            for i in range(self.layer.n_neurons):
                expected_output[i, b] = np.sum(
                    np.arange(s*i, s*(i+1))*np.arange(s*b, s*(b+1))
                    ) + i

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)
        
    def test_backprop_input_full(self):
        #define input
        bs = 8
        input = np.random.normal(size=(self.layer.input_size, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_input_full_multidi(self):
        #define input
        bs = 8
        input = np.random.normal(size=(self.layer.input_size//5, 5, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_bias_full(self):
        #define input
        bs = 8
        input = np.random.normal(size=(self.layer.input_size, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #do forward pass
        nn_output = self.layer.forward(np.copy(input))

        #define previous gradient
        prev_grad = np.random.normal(size=nn_output.shape)

        #calculate the gradients
        self.layer.gradient(prev_grad)

        #get expected output
        jac = numerical_jac_params(self.layer, input, 'bias')
        expected_output = np.dot(jac.T, prev_grad.flatten())
        expected_output = expected_output.reshape(self.layer.biases.shape)

        #compare the results
        result, message = compare(self.layer.grad_biases, expected_output)
        self.assertTrue(result, message)

    def test_backprop_weights_full(self):
        bs = 8
        #np.random.seed(0)
        input = np.random.normal(size=(self.layer.input_size, bs))

        #set random weights
        weightshape = self.layer.weights.shape
        self.layer.weights = np.random.normal(size=weightshape)

        #do forward pass
        nn_output = self.layer.forward(np.copy(input))

        #define previous gradient
        prev_grad = np.random.normal(size=nn_output.shape)

        #calculate the gradients
        self.layer.gradient(prev_grad)
        
        #get expected output
        jac = numerical_jac_params(self.layer, input, 'weight')
        expected_output = np.dot(jac.T, prev_grad.flatten())
        expected_output = expected_output.reshape(self.layer.weights.shape)

        #compare the results
        result, message = compare(self.layer.grad_weights, expected_output)
        self.assertTrue(result, message)

class ModelTestMaxpool(unittest.TestCase):

    def setUp(self):
        self.layer = model.MaxpoolLayer(
            size = 2
        )

    def test_range_max(self):
        #define input
        input = np.arange(60).reshape(10, 6, 1, 1)

        #define expected output
        expected_output = np.array([
            [7, 9, 11],
            [19, 21, 23],
            [31, 33, 35],
            [43, 45, 47],
            [55, 57, 59]
        ])
        expected_output = expected_output.reshape(expected_output.shape+(1, 1))

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_range_square_max(self):
        #define input
        input = np.arange(64).reshape(8, 8, 1, 1)

        #define expected output
        firstrow = np.array([9, 11, 13, 15])
        expected_output = np.array([firstrow+i*16 for i in range(4)])
        expected_output = expected_output.reshape((4, 4, 1, 1))

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_range_square_batch_max(self):
        #define batch size
        bs = 8

        #define input
        input = np.arange(64).reshape(8, 8, 1, 1)
        input = np.repeat(input, bs, axis = 3)

        #define expected output
        firstrow = np.array([9, 11, 13, 15])
        expected_output = np.array([firstrow+i*16 for i in range(4)])
        expected_output = expected_output.reshape((4, 4, 1, 1))
        expected_output = np.repeat(expected_output, bs, axis = 3)

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_singletile_max(self):
        #set input
        bs = 1
        input = np.random.normal(size=(2, 2, 1, bs))

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_bigger_max(self):
        #define input
        bs = 16 
        input = np.random.normal(size=(6, 4, 4, bs))

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

class ModelTestReLu(unittest.TestCase):

    def setUp(self):
        self.layer = model.ReLuLayer()
        #define batch size
        self.bs = 8

    def test_range_relu(self):
        #define input
        input = np.arange(64).reshape(64, 1) - 31.5
        input = np.repeat(input, self.bs, axis = 1)

        #define expected output
        expected_output = np.zeros(64)
        expected_output[32:] = input[32:,0]
        #reshape for batch stuff
        expected_output = expected_output.reshape(64, 1)
        expected_output = np.repeat(expected_output, self.bs, axis = 1)

        output = self.layer.forward(input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_backprop_relu(self): 
        input = np.random.normal(size=(30, self.bs))

        #calculate the gradients
        output, expected_output = get_input_gradients(self.layer, input)

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

class Test_speed(unittest.TestCase):

    def setUp(self):
        self.maxtime = 1e-2

    def setup(self, layer, forward = False):
        setup = ('import model;import numpy as np;'+
        f'layer = model.{layer};'+
        'input=np.random.normal(size=(14, 14, 3, 16))')
        if forward:
            setup = setup + ';layer.forward(input)'
        return setup

    def test_speed_forward_conv(self):
        code = 'layer.forward(input)'
        layer = 'ConvLayer(n_filters=6,input_depth=3,kernel_size=5)'
        setup = self.setup(layer, False)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

    def test_speed_backward_conv(self):
        code = 'layer.gradient(np.ones((10, 10, 6, 16)))'
        layer = 'ConvLayer(n_filters=6,input_depth=3,kernel_size=5)'
        setup = self.setup(layer, True)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

    def test_speed_forward_full(self):
        code = 'layer.forward(input)'
        layer = 'FullLayer(n_neurons=120,input_size=588)'
        setup = self.setup(layer, False)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

    def test_speed_backward_full(self):
        code = 'layer.gradient(np.ones((120, 16)))'
        layer = 'FullLayer(n_neurons=120,input_size=588)'
        setup = self.setup(layer, True)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

    def test_speed_forward_maxpool(self):
        code = 'layer.forward(input)'
        layer = 'MaxpoolLayer(size=2)'
        setup = self.setup(layer, False)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

    def test_speed_backward_maxpool(self):
        code = 'layer.gradient(np.ones((7, 7, 3, 16)))'
        layer = 'MaxpoolLayer(size=2)'
        setup = self.setup(layer, True)
        #make sure the setup is always fresh
        timer = timeit.Timer(stmt=code, setup=setup)
        time = np.mean(timer.repeat(repeat=20, number=1))
        self.assertLess(time, self.maxtime, 'too slow')

class ModelTestUpdate(unittest.TestCase):

    def setUp(self):
        self.insize = 200
        self.n_out = 10
        #define layers
        layers = [
            #full layer
            model.FullLayer(n_neurons=self.n_out,
            input_size=self.insize,
            ini_bias=0.1,
            weight_iniscale=0.0001),
            #Relu
            model.ReLuLayer()
        ]

        self.model = model.CNN(layers, learning_rate=1e-4)

        self.bs = 8

        #random input
        self.input = np.random.normal(size=(self.insize, self.bs))
        #define random labels
        self.truelabel = np.zeros((self.n_out, self.bs))
        possible_labels = np.arange(self.n_out, dtype=int)
        random_ints = np.random.choice(possible_labels, size=(self.bs))
        self.truelabel[random_ints, np.arange(self.bs)] = 1

    def test_update_loss(self):
        self.model.output = np.ones((self.n_out, self.bs)) / self.n_out
        output = self.model.loss_crossentropy(self.truelabel)

        expected_output = - np.log(1/self.n_out)*self.bs

        #compare the results
        result, message = compare(output, expected_output)
        self.assertTrue(result, message)

    def test_update_gradient(self):
        #forward pass
        self.model.forward(self.input)

        #calculate gradient of loss
        output = self.model.gradient_crossentropy(self.truelabel)

        #do numerical derivation
        stepsize = 1e-5

        num_params = self.model.output.size
        param_shape = self.model.output.shape
        original_param = np.copy(self.model.output)

        grad = np.zeros((param_shape))

        center = self.model.loss_crossentropy(self.truelabel)
        #loop over parameters
        for i in range(num_params):
            step = np.zeros(param_shape)
            #define infinitersimal element
            step.flat[i] = stepsize
            #update parameters
            self.model.output = original_param - step
            #calculate loss
            leftside = self.model.loss_crossentropy(self.truelabel)
            #update parameters
            self.model.output = original_param + step
            #do backward pass
            rightside = self.model.loss_crossentropy(self.truelabel)
            #construct samples
            samples = np.array([leftside, center, rightside])
            #do gradient and take center
            grad.flat[i] = np.gradient(samples, stepsize, axis=0)[0]

        #compare the results
        result, message = compare(output, grad)
        self.assertTrue(result, message)

    def test_update_onefulllayer_biases(self):
        #forward pass
        self.model.forward(self.input)

        #calculate gradients
        self.model.calculate_gradients(self.truelabel)

        #get expected output
        expected_output = numerical_grad_params_loss(
            model=self.model,
            layer=0,
            input=self.input,
            truelabels=self.truelabel,
            paramtype='bias')

        output = self.model.layers[0].grad_biases
        #compare the results
        result, message = compare(output, expected_output, accuracy = 2e-5)
        self.assertTrue(result, message)

    def test_update_onefulllayer_weights(self):
        #forward pass
        self.model.forward(self.input)

        #calculate gradients
        self.model.calculate_gradients(self.truelabel)

        #get expected output for gradient
        expected_output = numerical_grad_params_loss(
            model=self.model,
            layer=0,
            input=self.input,
            truelabels=self.truelabel,
            paramtype='weight')
        #reshape
        weightshape = self.model.layers[0].weights.shape
        expected_output = expected_output.reshape(weightshape)

        #get gradients
        output = self.model.layers[0].grad_weights

        #compare the results
        result, message = compare(output, expected_output, accuracy = 2e-5)
        self.assertTrue(result, message)

if __name__ == "__main__":
    unittest.main()
